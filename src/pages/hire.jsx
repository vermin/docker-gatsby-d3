import { Link as GatsbyLink } from 'gatsby';
import React from 'react';
import { Container, Heading, Link, Text } from 'rebass';
import Layout from '../components/Layout';
import styled from 'styled-components';

import { what } from '../data/about';

const StyledHeading = styled(Heading)`
  // border: 4px solid black;
  // transform: rotate(-5deg)
`

const HirePage = () => (
  <Layout>
    <Container>
      <StyledHeading my={3}>Hire me</StyledHeading>
      <Text is="p">Now go build something great.</Text>
      <Text is="p" my={3}>
        <Link is={GatsbyLink} to="/page-2/">
          Go to page 2
        </Link>
      </Text>
      { what.map( section => 
        <div>
          <Text key={section.title}>{section.title}</Text>
          <ul>
            { section.entries.map( entry =>
              <li key={entry.name.key}>
                {"link" in entry ? <a href={entry.link}>{entry.name}</a> : entry.name}
              </li>
            )}
          </ul>
        </div>
      )} 
    </Container>
  </Layout>

);

export default HirePage;
