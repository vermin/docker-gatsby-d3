module.exports = {
  siteMetadata: {
    title: 'awst.in',
    language: 'en',
  },
  plugins: [
    'gatsby-plugin-react-helmet', 
    'gatsby-plugin-styled-components',
    'gatsby-transformer-remark',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages/`,
        name: "pages",
      },
    }
  ],
};
